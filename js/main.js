
Vue.component('message', {
    props: ['title', 'body'],
    template: `
        <article class="message is-dark">
            <div class="message-header">
                {{ title }}
                <button class="delete" aria-label="delete"></button>
            </div>
            <div class="message-body">{{ body }}</div>
        </article>
    `
});

new Vue({
    el: '#root',
    data: {
        message: 'Hello world'
    }
});
